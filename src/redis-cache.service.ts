import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService {
    constructor(
        @Inject(CACHE_MANAGER) private readonly cache: Cache,
    ) {}

    async get(key): Promise<string> {
        const value =  await this.cache.get(key);
        return value;
    }

    async set(key: string, value: string, time: number) {
        if (time) {
            await this.cache.set(key, value, {ttl: time});
        } else {
            await this.cache.set(key, value, {ttl: 1000})
        }
    }

    async del(key: string) {
        await this.cache.del(key);
    }
}
