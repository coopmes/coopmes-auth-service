import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import {MessagePattern} from "@nestjs/microservices";
import {RedisCacheService} from "./redis-cache.service";
import {JwtService} from "@nestjs/jwt";


@Controller()
export class AppController {
  constructor(
      private readonly appService: AppService,
      private readonly redisService: RedisCacheService,
      private readonly jwtService: JwtService
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  async generateToken(user: object): Promise<string> {
    return this.jwtService.sign(user);
  }

  @MessagePattern({ cmd: 'generate_token' })
  async signIn(user: object): Promise<string> {
      const jwt = await this.generateToken(user);

      const minute = 60;
      const hours = minute * 60;
      const day = hours * 24;

      await this.redisService.set(jwt, JSON.stringify(user), day);
      return jwt;
  }

  @MessagePattern({ cmd: 'check_token' })
  async checkToken(jwt: string): Promise<boolean> {
    if (!await this.jwtService.verifyAsync(jwt)) {
        return false;
    }

    const userJson = await this.redisService.get(jwt);
    return Boolean(userJson);
  }

  @MessagePattern({ cmd: 'out_login' })
  async outLogin(jwt: string): Promise<boolean> {
      const isCheck = await this.checkToken(jwt);

      if (!isCheck) {
        return false;
      }

      await this.redisService.del(jwt);

      return true;
  }

  @MessagePattern({ cmd: 'get_user_id' })
  async getUserId(jwt: string): Promise<string> {
      if (!jwt) {
          return '';
      }

      if (!await this.jwtService.verifyAsync(jwt)) {
          return '';
      }

      const userJson = await this.redisService.get(jwt);

      if (!userJson) {
          return '';
      }

      return JSON.parse(userJson).userId || '';
  }
}
